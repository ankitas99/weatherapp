import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';
import { City } from '../../model/city';
import { WeatherService } from '../../services/weather.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  cities: string[];
  cityList: City[];

  /**
   * Creates an instance of HomeComponent
   *
   * @param {Router} router
   * @param {WeatherService} weatherService
   */
  constructor(private router: Router, private weatherService: WeatherService) {

  }

  /**
   * ngOnInit, get called after HomeComponent initialised!
   */
  ngOnInit(): void {
    //Top 5 Cities to visit in Europe
    this.cities = ['Barcelona', 'Helsinki', 'Florence', 'Prague', 'Paris'];
    if (isNullOrUndefined(this.cityList)) {
      this.cityList = [];
      this.getWeatherDataByCityName();
    }
  }

  /**
   * Fetching current weather data for city list
   */
  getWeatherDataByCityName() {
    for (let i = 0; i < this.cities.length; i++) {
      this.weatherService.getWeatherDataByCityName(this.cities[i]).subscribe((data: any) => {
        this.cityList.push(data);
      },
        error => {
          console.log("Error : ", error);
        })
    }
  }

  /**
   * Navigating to city details page
   *
   * @param {City} city
   */
  getCityDetails(city: City) {
    this.router.navigate(['/city', city.name]);
  }

}
