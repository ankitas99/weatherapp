import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { City } from 'src/app/model/city';
import { WeatherService } from 'src/app/services/weather.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})
export class CityComponent implements OnInit {
  name: string;
  sub: any;
  city: City;
  cityList: City[];

  /**
   * Creates an instance of CityComponent
   *
   * @param {ActivatedRoute} route
   * @param {WeatherService} weatherService
   */
  constructor(private route: ActivatedRoute, private weatherService: WeatherService) { }

  /**
   * ngOnInit, get called after CityComponent initialised!
   */
  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      //fetching city name from route params
      this.name = params['id'];
      if (isNullOrUndefined(this.cityList)) {
        this.cityList = [];
        this.getCityByName();
      }
    });
  }


  /**
   * Fetching the 5 days weather forecast details by city name
   */
  getCityByName() {
    this.weatherService.getDailyForecast(this.name).subscribe(data => {
      data.list.forEach(element => {
        let date = new Date(element.dt_txt);
        element.day = date;
        if (date.getHours() === 9) {
          this.cityList.push(element);
        }
      });
    },
      error => {
        console.log("Error : ", error);
      });
  }

}
