import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { City } from '../model/city';
import * as App from '../resources/appConfig.json'; // contains server endpoints url


@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  appid: string = '3d8b309701a13f65b660fa2c64cdc517';
  cityList: City[];

  /**
   *
   * @param {HttpClient} httpClient
   */
  constructor(private httpClient: HttpClient) {

  }


  /**
   * API call to fetch current weather details by city name
   *
   * @param {string} cityName
   */
  getWeatherDataByCityName(cityName: string) {
    const headers = new HttpHeaders();
    headers.append('Accept', 'application/json');
    let params = new HttpParams();
    params = params.append('q', cityName);
    params = params.append('units', 'metric');
    params = params.append('appid', this.appid);
    return this.httpClient.get<City>(App.serverUrl + App.current, {
      headers,
      params
    });
  }

  /**
   * API call to fetch weather forecast details by city name
   *
   * @param {string} cityName
   */
  getDailyForecast(cityName: string) {
    const headers = new HttpHeaders();
    headers.append('Accept', 'application/json');
    let params = new HttpParams();
    params = params.append('q', cityName);
    params = params.append('units', 'metric');
    params = params.append('appid', this.appid);
    return this.httpClient.get<any>(App.serverUrl + App.forecast, {
      headers,
      params
    });
  }
}
